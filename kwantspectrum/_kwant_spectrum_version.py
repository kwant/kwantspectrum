# This file will be overwritten by setup.py when a source or binary
# distribution is made.
version = "__use_git__"

# These values are only set if the distribution was created with 'git archive'
refnames = "$Format:%D$"
git_hash = "$Format:%h$"
